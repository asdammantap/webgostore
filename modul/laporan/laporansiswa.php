<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<div class="row-fluid">
                       <div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="?p=home">Dashboard</a> <span class="divider">/</span> <a href="#">Proses Cetak Laporan</a>
	                                    </li>
	                                 </ul>
                            	</div>
                        	</div>
                    	</div>
 <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Proses Cetak Laporan Siswa</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
					<!-- BEGIN FORM-->
					<form method="post" action="modul/laporan/prosesviewlapsiswa.php" class="form-horizontal">
						<fieldset>
							<div class="control-group">
  								<label class="control-label">Pilih Laporan yang Akan Dicetak<span class="required">*</span></label>
  								<div class="controls">
  									<select class="span6 m-wrap" name="lapsiswa">
  										<option value="">--Pilih Laporan--</option>
										 <option>Laporan Siswa Keseluruhan</option>
										 <option>Laporan Siswa Per-Kelas</option>
  									</select>
  								</div>
  							</div>
							<div class="form-actions">
  								<input type="submit" class="btn btn-primary" value="Simpan">
  								<button type="button" class="btn">Batal</button>
  							</div>
						</fieldset>
					</form>
					<!-- END FORM-->
				</div>
			    </div>
			</div>
                     	<!-- /block -->
		    </div>
                     <!-- /validation -->
</body>
</html>